import * as React from "react";
import * as ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import "primereact/resources/themes/bootstrap4-light-blue/theme.css";
import 'primereact/resources/primereact.css';
import "primeicons/primeicons.css";                                
import App from "./App";
import "./index.css";
import "primereact/resources/themes/bootstrap4-light-blue/theme.css";
import 'primereact/resources/primereact.css';

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById("root") as HTMLElement
);
