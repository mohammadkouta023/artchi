import React, { useState } from "react";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { useNavigate } from "react-router-dom";
import "./css/login.css";


const Login = (props: any) => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [selectedECommerceApp, setSelectedECommerceApp] = useState(null);
  const cities = [
    { name: "New York", code: "NY" },
    { name: "Rome", code: "RM" },
    { name: "London", code: "LDN" },
  ];
  const onECommerceAppChange = (e) => {
    setSelectedECommerceApp(e.value);
  };
  let navigate = useNavigate();

  const onLoginHandler = () => {
    navigate("/welcome", {
      state: {
        email: email,
        password: password,
      },
    });
  };

  return (
    <React.Fragment>
      <div className="login">
        <h1 style={{color:"#83AAE5",fontWeight:"normal"}}>Login to your Architecture Guide</h1>
        <div className="login-form">
          <InputText
            id="email"
            value={email}
            placeholder="Email"
            onChange={(e) => setEmail(e.target.value)}
            className="inputs"
          />
          <br />
          <InputText
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Password"
            className="inputs"
          />
          <br />
        <Button
          label="Login"
          onClick={onLoginHandler}
          className="login-button"
        />
        </div>
      </div>
    </React.Fragment>
  );
};

export default Login;
