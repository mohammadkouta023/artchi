import React, { useEffect, useState } from "react";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { useNavigate } from "react-router-dom";
import "./css/myProjects.css";

const MyProjects = () => {
  let navigate = useNavigate();
  const [customers1, setCustomers1] = useState([
    {
      projectName: "CEVA Site Classification",
      description: " Description",
      createdBy: "Karine Lamaa",
      createdDate: "2015-12-01T00:00:00Z",
      status: "Pending",
    },
    {
      projectName: "CMA CGMCatalog Api",
      description: "Description",
      createdBy: "Ali Hamka",
      createdDate: "2021-13-01T00:00:00Z",
      status: "Approved",
    },
  ]);

  const viewProject = (rowData: any) => {
    console.log({ state: { rowData } });
    navigate("/projectOverview", { state: { rowData } });
  };

  const actions = (rowData: any): JSX.Element => {
    const hideButtons = (projectName: string) => {
      (document.getElementById(projectName + "-approve") as any).disabled =
        true;
      (document.getElementById(projectName + "-disapprove") as any).disabled =
        true;
    };

    return (
      <React.Fragment>
        <div className="p-grid">
          <div className="p-col">
            <Button
              type="button"
              icon="pi pi-eye"
              title="view"
              className="p-button-text"
              onClick={(e) => viewProject(rowData)}
            />
            <Button
              id={rowData.projectName + "-approve"}
              type="button"
              icon="pi pi-thumbs-up"
              title="Approve"
              className="p-button-text"
              onClick={() => hideButtons(rowData.projectName)}
            />
            <Button
              id={rowData.projectName + "-disapprove"}
              type="button"
              icon="pi pi-thumbs-down"
              title="Disapprove"
              className="p-button-text"
              hidden={true}
              onClick={() => hideButtons(rowData.projectName)}
            />
          </div>
        </div>
      </React.Fragment>
    );
  };

  const statusTemplate = (rowData: any): JSX.Element => {
    const statusColor: string = rowData.status === "Pending" ? "red" : "green";
    return (
      <React.Fragment>
        <div style={{ color: statusColor }}>{rowData.status}</div>
      </React.Fragment>
    );
  };

  const onHomeHandler = () => {
    navigate("/welcome");
  };

  return (
    <div className="myProjects">
      <Button className="rdd-home" icon="pi pi-home" onClick={onHomeHandler} />
      <h1 style={{ color: "#83AAE5", fontWeight: "normal" }}>
        View ALL Projects
      </h1>
      <div className="datatable-ctr">
        <DataTable
          value={customers1}
          paginator
          responsiveLayout="scroll"
          paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown"
          currentPageReportTemplate="Showing {first} to {last} of {totalRecords}"
          rows={10}
          rowsPerPageOptions={[10, 20, 50]}
        >
          <Column
            field="projectName"
            header="Project Name"
            style={{ width: "16%" }}
          ></Column>
          <Column
            field="description"
            header="Description"
            style={{ width: "16%" }}
          ></Column>
          <Column
            field="createdBy"
            header="Created By"
            style={{ width: "16%" }}
          ></Column>
          <Column
            field="createdDate"
            header="Created Date"
            style={{ width: "16%" }}
          ></Column>
          <Column
            header="Status"
            body={statusTemplate}
            style={{ width: "16%" }}
          ></Column>
          <Column
            body={actions}
            header="Actions"
            style={{ width: "20%" }}
          ></Column>
        </DataTable>
      </div>
      <div className="telescope">
        <img
          src={window.location.origin + "/assets/images/telescope.png"}
          alt={"telescope"}
          className="telescope-image"
        />
      </div>
    </div>
  );
};

export default MyProjects;
