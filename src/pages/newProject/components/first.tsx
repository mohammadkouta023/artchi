import { InputText } from "primereact/inputtext";
import React from "react";
import { useState } from "react";
import { InputTextarea } from "primereact/inputtextarea";
import { Dropdown } from "primereact/dropdown";
import { Calendar } from "primereact/calendar";
import { Button } from "primereact/button";
import styles from "../css/first.module.css";

const First = (props: any) => {
  const [projectName, setProjectName] = useState<string>("");
  const [projectOverView, setProjectOverView] = useState<string>("");
  const [selectedECommerceApp, setSelectedECommerceApp] = useState(null);
  const [finance, setFinance] = useState(null);
  const [expectedDeliveryDate, setExpectedDeliveryDate] = useState("");
  const [neededResources, setNeededResources] = useState("");
  const [rfcNumber, setRfcNumber] = useState("");
  const [businessEmail, setBusinessEmail] = useState("");
  const [technicalEmail, setTechnicalEmail] = useState("");
  const appType = [
    { name: "Internal", code: "I" },
    { name: "Ecommerce", code: "Ecom" },
    { name: "Management", code: "M" },
  ];

  const finances = [
    { name: "Finance", code: "F" },
    { name: "Logistics", code: "L" },
    { name: "Audit", code: "A" },
    { name: "Marketing", code: "MK" },
    { name: "Ecommerce", code: "Ecom" },
  ];
  const onECommerceAppChange = (e) => {
    setSelectedECommerceApp(e.value);
  };
  const onFinanceChange = (e) => {
    setFinance(e.value);
  };
  return (
    <React.Fragment>
      <div className="card">
        <div className="flex-container" id={styles.container}>
          <div className="flex-container" id={styles.leftColumn}>
            <span className="p-float-label">
              <InputText
                id={styles.projectName}
                value={projectName}
                onChange={(e) => setProjectName(e.target.value)}
                placeholder="Project Name"
              />
            </span>
            <br />
            <span className="p-float-label">
              <InputTextarea
                id={styles.projectOverView}
                rows={5}
                cols={30}
                value={projectOverView}
                onChange={(e) => setProjectOverView(e.target.value)}
                autoResize
                placeholder="Project Overview"
              />
            </span>
            <br />
            <span className="p-float-label">
              <Dropdown
                id={styles.dropdownCity}
                value={selectedECommerceApp}
                options={appType}
                onChange={onECommerceAppChange}
                optionLabel="name"
                placeholder="Application Type"
              />
            </span>
            <br />
            <span className="p-float">
              <Calendar
                id={styles.expectedDeliveryDate}
                value={new Date(expectedDeliveryDate)}
                // onChange={(e) => setExpectedDeliveryDate(e.value)}
                placeholder="Expected Delivery Date"
              />
            </span>
            <br />
            <span className="p-float-label">
              <InputText
                id={styles.neededResources}
                value={neededResources}
                onChange={(e) => setNeededResources(e.target.value)}
                placeholder="#Needed Resources"
              />
            </span>
            <br />
            <span className="p-float-label">
              <Dropdown
                id={styles.dropdownFinance}
                value={finance}
                options={finances}
                onChange={onFinanceChange}
                optionLabel="name"
                placeholder="Business Unit"
              />
            </span>
          </div>

          <div className="flex-container" id="rightColumn">
            <span className="p-float-label">
              <InputText
                id={styles.rfcNumber}
                value={rfcNumber}
                onChange={(e) => setRfcNumber(e.target.value)}
                placeholder="RFC Number"
              />
            </span>
            <br />
            <label id={styles.businessMessage}>
              Who should I contact for business?
            </label>
            <span className="p-float-label">
              <InputText
                id={styles.businessEmail}
                value={businessEmail}
                onChange={(e) => setBusinessEmail(e.target.value)}
                placeholder="Business Email"
              />
            </span>
            <br />
            <label id={styles.technicalMessage}>
              Who should I contact for technical?
            </label>
            <span className="p-float-label">
              <InputText
                id={styles.technicalEmail}
                value={technicalEmail}
                onChange={(e) => setTechnicalEmail(e.target.value)}
                placeholder="Technical Email"
              />
            </span>
            <br />
          </div>
        </div>
        <br />
        <img
          src="/assets/images/telescope.png"
          alt="telescope"
          className={styles.telescopeImg}
        />
      </div>
    </React.Fragment>
  );
};

export default First;
