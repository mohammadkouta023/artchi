import { Button } from "primereact/button";
import React, { useState } from "react";
import { ListBox } from "primereact/listbox";

const Four = (props: any) => {
  const [selectedCountries, setSelectedCountries] = useState(null);
  const countries = [
    { name: "Australia", code: "AU" },
    { name: "Brazil", code: "BR" },
    { name: "China", code: "CN" },
  ];
  const countryTemplate = (option) => {
    return (
      <div className="country-item">
        <div>{option.name}</div>
      </div>
    );
  };
  return (
    <React.Fragment>
      <div>
        <div className="card">
          <p>Here is your suggested Diagram</p>
          <img
            src={window.location.origin + "/assets/images/diagram.png"}
            alt={"diagram"}
            className="product-image"
          />
          <p>Links to download templates</p>
          <ListBox
            value={selectedCountries}
            options={countries}
            onChange={(e) => setSelectedCountries(e.value)}
            multiple
            filter
            optionLabel="name"
            itemTemplate={countryTemplate}
            style={{ width: "15rem" }}
            listStyle={{ maxHeight: "250px" }}
          />
          <p>Approved</p>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Four;
