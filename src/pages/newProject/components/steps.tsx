import React, { useState, useRef } from "react";
import { Button } from "primereact/button";
import "../css/step.css";

const StepsComponent = (props: any) => {
  const {setPage, page} = props;

  return (
    <React.Fragment>
    <div className="steps">
      <div className="step" 
        onClick = {() => setPage(0)}
        style={{ backgroundColor: (page === 0)? "#1E4072" : ""}}
      ></div>
      <div className="step" 
        onClick = {() => setPage(1)}
        style={{ backgroundColor: (page === 1)? "#1E4072" : ""}}
      ></div>
      <div className="step" 
        onClick = {() => setPage(2)}
        style={{ backgroundColor: (page === 2)? "#1E4072" : ""}}
      ></div>
      <div className="step" 
        onClick = {() => setPage(3)}
        style={{ backgroundColor: (page === 3)? "#1E4072" : ""}}
      ></div>
    </div>

    </React.Fragment>
  );
};

export default StepsComponent;
