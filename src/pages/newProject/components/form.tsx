import { Button } from "primereact/button";
import Steps from "./steps";
import First from "./first";
import Four from "./four";
import Second from "./second";
import Third from "./third";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import '../css/form.css';

const Form = (props: any) => {
  const [page, setPage] = useState<number>(0);
  let navigate = useNavigate();
  let steps = [
    { title: "Ready to get Introduced?" },
    { title: "How about the Technical Part?"},
    { title: "Go it... lets go to work" },
    { title: "You're ready to go!" },
  ]; 



  const onHomeHandler = () => {
    navigate("/welcome");
  };

    return (
      <div>
        <Button className="home-button" icon="pi pi-home" onClick={onHomeHandler} />
        
        <div style={{position:"relative"}}>
          {page === 0 && <h1 className="form-header">{steps[0].title}</h1>}
          {page === 1 && <h1 className="form-header">{steps[1].title}</h1>}
          {page === 2 && <h1 className="form-header">{steps[2].title}</h1>}
          {page === 3 && <h1 className="form-header">{steps[3].title}</h1>}
          <Steps setPage = {setPage} page={page}/>
        </div>
          {page === 0 && <First/>}
          {page === 1 && <Second/>}
          {page === 2 && <Third/>}
          {page === 3 && <Four/>}
      </div>
    );
}

export default Form;
