import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import React, { useState } from "react";
import { ListBox } from "primereact/listbox";
import "../css/third.css";
import { Link } from "react-router-dom";

const Third = (props: any) => {
  const openInNewTab = (url) => {
    window.open(url, "_blank", "noopener,noreferrer");
  };

  const [architectureEmail, setArchitectureEmail] = useState("");
  const usefulLinks = [
    {
      name: "Cassandra Documentation",
      url: "https://cassandra.apache.org/doc/latest/",
    },
    { name: "Cron Sync", url: "https://www.cronsync.com/" },
    {
      name: "Difference Engine",
      url: "https://en.wikipedia.org/wiki/Difference_engine#:~:text=A%20difference%20engine%20is%20an,first%20created%20by%20Charles%20Babbage.",
    },
  ];

  const listLinks = usefulLinks.map((link) => (
    <li id="linkListItem">
      <button onClick={(e) => openInNewTab(link.url)} className="linkButton">
        {link.name}
      </button>
    </li>
  ));

  const email = "john.doe@cma-cgm.com";
  return (
    <React.Fragment>
      <div>
        <div className="card">
          <div className="flex-container" id="container">
            <div id="leftColumn">
              <p>Here is your suggested Diagram</p>
              <img
                src={window.location.origin + "/assets/images/diagram.png"}
                alt={"diagram"}
                className="product-image"
                id="product-image"
              />
            </div>
            <div>
              <p className="textClass">
                It's currently being reviewed by our architect:
              </p>
              <button
                onClick={() =>
                  (window.location.href = "mailto:yourmail@gmail.com")
                }
                id="emailButton"
              >
                {email}
              </button>
              <span className="p-float-label">
                <br></br>
              </span>
              <p>
                Meanwhile you can check below contacts/URLs:
              </p>
              <ul id="linksList">{listLinks}</ul>
              <p id="projectStatus">Pending Approval</p>
            </div>
          </div>
        </div>{" "}
      </div>
    </React.Fragment>
  );
};

export default Third;
