import { InputText } from "primereact/inputtext";
import React from "react";
import { useState } from "react";
import { Dropdown } from "primereact/dropdown";
import { Button } from "primereact/button";
import { Checkbox } from "primereact/checkbox";
import { InputSwitch } from "primereact/inputswitch";
import { InputNumber } from "primereact/inputnumber";
import "../css/second.css";

const Second = (props: any) => {
  const [environment, setEnvironment] = useState<string>(null);
  const [database, setDatabase] = useState<string>(null);
  const [runTime, setRunTime] = useState<string>(null);
  const [frontEnd, setFrontEnd] = useState<Boolean>(false);
  const [backEnd, setBackEnd] = useState<Boolean>(false);
  const [externalProject, setExternalProject] = useState<Boolean>(false);
  const [sso, setSso] = useState<Boolean>(false);
  const [numberOfUsers, setNumberOfUsers] = useState<number>();
  const [architectureEmail, setArchitectureEmail] = useState<string>("");

  const environments = [
    { name: "CMA CGM", code: "CMA" },
    { name: "CMA CGM (Digital)", code: "CMA" },
    { name: "CEVA LOGISTICS", code: "CEVA" },
    { name: "CEVA LOGISTIS (Digital)", code: "CEVA" },
    { name: "CEVA LOGISTICS (Visibility)", code: "CEVA" },
  ];

  const databases = [
    { name: "SQL", code: "CMA" },
    { name: "NoSQL", code: "CEVA" },
    { name: "Relational", code: "CEVA" },
    { name: "N/A", code: "CEVA" },
  ];

  const times = [
    { name: "24 hour", code: "hour" },
    { name: "1 day", code: "day" },
    { name: "1 time use", code: "day" },
  ];

  const onDatabaseChange = (e) => {
    setDatabase(e.value);
  };

  const onRunTimeChange = (e) => {
    setRunTime(e.value);
  };

  const onEnvironmentChange = (e) => {
    setEnvironment(e.value);
  };

  return (
    <React.Fragment>
      <div>
        <div className="card">
          <div className="flex-container" id="container">
            <div className="flex-container" id="leftColumn">
              <br />
              <label id="projectEnvMessage">
                What's your project environment?
              </label>
              <Dropdown
                id="projectEnvDropdown"
                value={environment}
                options={environments}
                onChange={onEnvironmentChange}
                optionLabel="name"
                placeholder="Environment"
              />
              <br />
              <div className="field-checkbox">
                <Checkbox
                  inputId="frontEnd"
                  checked={frontEnd}
                  onChange={(e) => setFrontEnd(e.checked)}
                />
                <label htmlFor="frontEnd">FrontEnd</label>
              </div>
              <div className="field-checkbox">
                <Checkbox
                id="backendCheckbox"
                  inputId="backEnd"
                  checked={backEnd}
                  onChange={(e) => setBackEnd(e.checked)}
                />
                <label htmlFor="backEnd">BackEnd</label>
              </div>
              <p>
                Is your Project accessible externally?
                <InputSwitch
                  checked={externalProject}
                  onChange={(e) => setExternalProject(e.value)}
                />
              </p>
              <br />
              <InputNumber
                id="numberOfUsers"
                inputId="numberOfUsers"
                value={numberOfUsers}
                onValueChange={(e) => setNumberOfUsers(e.value)}
                placeholder="Number of Users"
              />
              <br />
              <label id="dbModelMessage">
                Which database model would you require?
              </label>
              <Dropdown
                id="dbModelDropdown"
                value={database}
                options={databases}
                onChange={onDatabaseChange}
                optionLabel="name"
                placeholder="Database Model"
              />
            </div>

            <div className="flex-container" id="rightColumn">
              <br />
              <label id="projectRunTimeMessage">For how many time?</label>
              <Dropdown
                id="projectRuntimeDropdown"
                value={runTime}
                options={times}
                onChange={onRunTimeChange}
                optionLabel="name"
                placeholder="Run Time"
              />
              <br />
              <label id="architectMessage">
                Working with a specific Architect?
              </label>
              <span className="p-float-label">
                <InputText
                  id="architectEmail"
                  value={architectureEmail}
                  onChange={(e) => setArchitectureEmail(e.target.value)}
                />
                <label htmlFor="architectureEmail">Architecture Email</label>
              </span>
              <p>
                Will you require SSO?
                <InputSwitch checked={sso} onChange={(e) => setSso(e.value)} />
              </p>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Second;
