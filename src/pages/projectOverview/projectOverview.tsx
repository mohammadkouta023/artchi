import React from "react";
import "./css/projectOverview.css";
import { useLocation } from "react-router-dom";
import { LocationState } from './model/projectOverview.model';

const ProjectOverview = (props: any) => {
  
  const location = useLocation();
  const rowData = (location.state as LocationState).rowData; 

  return (
    <React.Fragment>
      <div>
        <label>
          <span className="label-style">Description: </span>The project is
          developed to {rowData.projectName}...
        </label>
        <br />
        <label>
          <span className="label-style">Type:</span> Internal Application
        </label>
        <br />
        <label>
          <span className="label-style">Frontend Needed:</span> Yes
        </label>
        <br />
        <label>
          <span className="label-style">Backend Needed:</span> No
        </label>
        <br />
        <label>
          <span className="label-style">Business Contact:</span>{" "}
          Karine.lamaa@cevalogistics.com
        </label>
        <br />
        <label>
          <span className="label-style">Technical Contact:</span>{" "}
          Karine.lamaa@cevalogistics.com
        </label>
        <br />
        <label>
          <span className="label-style">External app:</span> Yes
        </label>
        <br />
        <label>
          <span className="label-style">Database Model:</span> Relational
        </label>
      </div>
    </React.Fragment>
  );
};

export default ProjectOverview;
