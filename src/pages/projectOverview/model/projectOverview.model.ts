export type LocationState = {
    rowData: LocationStateRowData
};

export type LocationStateRowData = {
    projectName: string,
    description: string,
    createdBy: string,
    createdData: Date,
    status:string
};