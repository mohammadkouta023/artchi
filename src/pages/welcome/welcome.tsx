import { Button } from "primereact/button";
import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import "./css/welcome.css";

const Welcome = (props: any) => {
  const { state } = useLocation();
  let navigate = useNavigate();

  const onNewProjectHandler = () => {
    navigate("/newProject");
  };

  const onViewMyProjectsHandler = () => {
    navigate("/myProjects");
  };

  return (
    <React.Fragment>
      <div>
        <div className="card">
          <div style={{marginTop:"80px"}}>
            <p className="welcome-text">
              Hello, I am Artchi your architecture captain <br /> How may i assist you ?
            </p>
          </div>
          <div className="buttons-box">
            <Button
              label="Create New Project"
              icon="pi pi-plus"
              onClick={onNewProjectHandler}
              className="welcome-buttons"
            />
            <br />
            <Button
              label="View My Projects"
              icon="pi pi-eye"
              className="welcome-buttons"
              onClick={onViewMyProjectsHandler}
            />
          </div>
          <img src="/assets/images/captain.png" alt="captain" className="captain-img" />
        </div>
      </div>
    </React.Fragment>
  );
};

export default Welcome;
