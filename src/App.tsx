import "./App.css";
import { Route, Routes } from "react-router-dom";
import Login from "./pages/login/login";
import Welcome from "./pages/welcome/welcome";
import NewProject from "./pages/newProject/newProject";
import MyProjects from "./pages/myProject/myProjects";
import ProjectOverview from "./pages/projectOverview/projectOverview";

const App = () => {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/login" element={<Login />} />
        <Route path="/welcome" element={<Welcome />} />
        <Route path="/newProject" element={<NewProject />} />
        <Route path="/myProjects" element={<MyProjects />} />
        <Route path="/projectOverview" element={<ProjectOverview />} />
      </Routes>
    </div>
  );
};

export default App;
